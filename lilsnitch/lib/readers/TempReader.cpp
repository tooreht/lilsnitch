#include "TempReader.h"

TempReader::TempReader(const string path_): path(path_) {
    inFile.open(path);
}

TempReader::~TempReader() {
    inFile.close();
}

float TempReader::read() {
    stringstream buffer;    
    buffer << inFile.rdbuf();
    inFile.clear();
    inFile.seekg(0, inFile.beg);
    int temp = stoi(buffer.str());
    return temp / 1000;
}
