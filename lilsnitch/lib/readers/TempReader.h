#ifndef TEMPREADER_H
#define TEMPREADER_H

#include <fstream>
#include <sstream>
#include <string>

using namespace std;

class TempReader {
    public:
        TempReader(const string path_);
        ~TempReader();
        float read();

    private:
        const string path;
        ifstream inFile;
};

#endif