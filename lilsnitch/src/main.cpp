#include <signal.h>

#include <chrono>
#include <thread>
#include <iostream>

#include "readers/TempReader.h"
#include "transport/TCPClient.h"

// ThinkPad X220
TempReader tr_cpu0("/sys/class/thermal/thermal_zone0/temp");
TempReader tr_cpu1("/sys/class/thermal/thermal_zone1/temp");

TCPClient tcp;

void sig_exit(int s)
{
	tcp.exit();
	exit(0);
}

int main()
{
    std::cout << "Lil Snitch" << std::endl;

    signal(SIGINT, sig_exit);
    tcp.setup("localhost", 6789);

    while (1) {
        float cpu0 = tr_cpu0.read();
        float cpu1 = tr_cpu1.read();
        std::cout << "CPU 0: " << cpu0 << "℃ , CPU 1: " << cpu1 << "℃" << std::endl;
		
        string msg = "{\"cpu0\":" + to_string(cpu0) + ",\"cpu1\":" + to_string(cpu1) + "}\n";
        std::cout << msg << std::endl;
        tcp.Send(msg);

        // std::this_thread::sleep_for(std::chrono::milliseconds(100));

		// string rec = tcp.receive();
		// if( rec != "" )
		// {
		// 	cout << rec << endl;
		// }

        std::this_thread::sleep_for(std::chrono::seconds(3));
    }

    return 0;
}