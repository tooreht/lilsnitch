import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from './core/data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent /* implements OnInit, OnDestroy */ {
  title = 'lilsnitch-client';

  tempCPU0: number = 0.0;
  tempCPU1: number = 0.0;

  threshold: number = 70;

  subscription: Subscription;

  constructor(private dataService: DataService) {}

  start() {
    this.subscription = this.dataService
      .connect('/topic/monitor/temp')
      .subscribe(payload => {
        // console.log(payload);
        this.tempCPU0 = payload.cpu0;
        this.tempCPU1 = payload.cpu1;
      });
      this.dataService.send("/app/init", JSON.stringify({"cpu0": 0.0, "cpu1": 0.0}));
  }

  stop() {
    this.subscription.unsubscribe();
    this.tempCPU0 = 0.0;
    this.tempCPU1 = 0.0;
  }

  // ngOnInit() {
  //   this.subscription = this.dataService
  //     .connect('/topic/greetings')
  //     .subscribe(payload => {
  //       this.tempCPU0 = payload.cpu0;
  //       this.tempCPU1 = payload.cpu1;
  //     });
  // }

  // ngOnDestroy() {
  //   this.subscription.unsubscribe();
  // }
}
