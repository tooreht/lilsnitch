
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { RxStomp } from '@stomp/rx-stomp';

const stompConfig = {
    // Typically login, passcode and vhost
    // Adjust these for your broker
    //   connectHeaders: {
    //     login: "guest",
    //     passcode: "guest"
    //   },
    // Broker URL, should start with ws:// or wss:// - adjust for your broker setup
    brokerURL: "ws://localhost:8080/ws",
    // Keep it off for production, it can be quit verbose
    // Skip this key to disable
    debug: function (str) {
        console.log('STOMP: ' + str);
    },
    // If disconnected, it will retry after 200ms
    reconnectDelay: 500,
};

@Injectable()
export class DataService {

    private rxStomp: RxStomp;

    constructor() {
        this.rxStomp = new RxStomp();
        this.rxStomp.configure(stompConfig);
    }

    connect(topic: string) {
        this.rxStomp.activate();
        return this.rxStomp.watch(topic).pipe(map(msg => JSON.parse(msg.body)));
    }

    send(topic: string, message: string) {
        this.rxStomp.publish({destination: topic, body: message});
    }
}
