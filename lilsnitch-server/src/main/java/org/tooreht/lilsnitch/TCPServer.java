package org.tooreht.lilsnitch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TCPServer {
    private static final Logger LOG = LoggerFactory.getLogger(TCPServerRunner.class);
    
    public interface TCPMessage {
        public void onMessage(String clientAddress, String message);
    }

    private ServerSocket server;
    private ArrayList<TCPMessage> subscribers;

    private TCPServer() throws UnknownHostException, IOException {
        this.subscribers = new ArrayList<TCPMessage>();
    }

    public TCPServer(String address) throws Exception {
        this();
        if (address != null && !address.isEmpty()) {
            this.server = new ServerSocket(0, 1, InetAddress.getByName(address));
        } else {
            this.server = new ServerSocket(0, 1, InetAddress.getLocalHost());
        }
    }

    public TCPServer(String address, int port) throws Exception {
        this();
        if (address != null && !address.isEmpty()) {
            this.server = new ServerSocket(port, 1, InetAddress.getByName(address));
        } else {
            this.server = new ServerSocket(port, 1, InetAddress.getLocalHost());
        }
    }

    public void listen() throws Exception {
        String message = null;
        Socket client = this.server.accept();
        String clientAddress = client.getInetAddress().getHostAddress();
        int clientPort = client.getPort();
        LOG.info("New connection from " + clientAddress + ":" + clientPort);

        BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));

        while ((message = in.readLine()) != null) {
            for (TCPMessage subscriber : subscribers) {
                subscriber.onMessage(clientAddress, message);
            }
        }
        LOG.info("Closing connection to " + clientAddress + ":" + clientPort);
        client.close();
    }

    public void listenForever() throws Exception {
        while (true) {
            this.listen();
        }
    }

    public InetAddress getSocketAddress() {
        return this.server.getInetAddress();
    }

    public int getPort() {
        return this.server.getLocalPort();
    }

    public void close() throws IOException {
        this.server.close();
    }

    public void subscribe(TCPMessage subscriber) {
        this.subscribers.add(subscriber);
    }
}