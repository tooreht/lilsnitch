package org.tooreht.lilsnitch;

public class TempMessage {

    private String cpu0;
    private String cpu1;
  
    public TempMessage() {
    }
  
    public TempMessage(String cpu0, String cpu1) {
      this.cpu0 = cpu0;
      this.cpu1 = cpu1;
    }
  
    public String getCPU0() {
      return cpu0;
    }
  
    public void setCPU0(String cpu0) {
      this.cpu0 = cpu0;
    }

    public String getCPU1() {
      return cpu1;
    }
  
    public void setCPU1(String cpu1) {
      this.cpu1 = cpu1;
    }
  }
