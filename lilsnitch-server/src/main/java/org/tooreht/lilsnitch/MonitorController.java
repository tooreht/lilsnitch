package org.tooreht.lilsnitch;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class MonitorController {

  @MessageMapping("/init")
  @SendTo("/topic/monitor/temp")
  public Monitor init(TempMessage message) throws Exception {
    Thread.sleep(1000); // simulated delay
    return new Monitor(message);
  }

}