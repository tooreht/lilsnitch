package org.tooreht.lilsnitch;

public class Monitor {

  private String cpu0;
  private String cpu1;

  public Monitor() {
  }

  public Monitor(TempMessage message) {
    this.cpu0 = message.getCPU0();
    this.cpu1 = message.getCPU1();
  }

  public String getCpu0() {
    return this.cpu0;
  }

  public String getCpu1() {
    return this.cpu1;
  }

}