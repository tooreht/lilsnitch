package org.tooreht.lilsnitch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.tooreht.lilsnitch.TCPServer.TCPMessage;

@Component
public class TCPServerRunner implements ApplicationRunner, TCPMessage {
    private static final Logger LOG = LoggerFactory.getLogger(TCPServerRunner.class);

    private TCPServer server;

    @Autowired
    private SimpMessagingTemplate brokerMessagingTemplate;

    public TCPServerRunner() throws Exception {
        this.server = new TCPServer("localhost", 6789);
        this.server.subscribe(this);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        LOG.info("Application started with option names : {}", args.getOptionNames());
        LOG.info("Listening for new TCP connections");

        try {
            this.server.listenForever();
        } finally {
            this.server.close();
        }

    }

    @Override
    public void onMessage(String clientAddress, String message) {
        LOG.info("Message from " + clientAddress + ": " + message);
        this.brokerMessagingTemplate.convertAndSend("/topic/monitor/temp", message);
    }
}
