# LilSnitch

This demo app is showcasing a C++ program reading CPU temperatures, sending them to a Java Spring Boot app over plain TCP, which itself sends the temperatures to an Angular app via websocket connection.

## Usage

### C++ Temp Reader

```bash
cd lilsnitch
make
```

### Java Spring Boot Backend

```bash
cd lilsnitch-server
gradle bootRun
```

### AngularJS Frontend

```bash
cd lilsnitch-client
ng server
```
